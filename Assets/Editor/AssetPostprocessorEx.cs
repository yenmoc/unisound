﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;

// ReSharper disable CheckNamespace
namespace UnityModule.Sound
{
    public class AssetPostprocessorEx : AssetPostprocessor
    {
        private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        {
#if SOUND_NAME_ENABLE
            var check = false;
            CheckingExist(importedAssets, ref check);
            CheckingExist(deletedAssets, ref check);
            CheckingExist(movedAssets, ref check);
            CheckingExist(movedFromAssetPaths, ref check);

            if (!check) return;
            SoundGenerate.GenerateSoundName();
#if UNITY_EDITOR
            UnityEngine.Debug.Log("Generate SoundName Success!");
#endif
#endif
        }

        private static void CheckingExist(IEnumerable<string> movedFromAssetPaths, ref bool check)
        {
            if (check) return;
            if (movedFromAssetPaths.Any(IsMatchFileName))
            {
                check = true;
            }
        }

        private static bool IsMatchFileName(string filename)
        {
            return Regex.IsMatch(filename, SoundDefine.PathBgmSourceFolder + @"/([^/]+/)*[^\.]+\.*")
                   || Regex.IsMatch(filename, SoundDefine.PathSoundEffectSourceFolder + @"/([^/]+/)*[^\.]+\.*");
        }
    }
}