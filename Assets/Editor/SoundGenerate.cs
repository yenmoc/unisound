﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using System.Text;
using UnityModule.EditorUtility;

namespace UnityModule.Sound
{
    public static class SoundGenerate
    {
        private const string DISABLE_CHECK_NAMESPACE = "// ReSharper disable CheckNamespace";
        private const string DISABLE_CHECK_INCONSISTENTNAMING = "// ReSharper disable InconsistentNaming";

        private static List<AudioClip> GetListBgm()
        {
            if (!Directory.Exists(SoundDefine.PathBgmSourceFolder))
            {
                return new List<AudioClip>();
            }

            var fileEntriesBgm = Directory.GetFiles(SoundDefine.PathBgmSourceFolder, "*", SearchOption.AllDirectories);
            return (from t in fileEntriesBgm select ConvertSystemPathToUnityPath(t) into filePath select AssetDatabase.LoadAssetAtPath(filePath, typeof(object)) into obj where obj where obj is AudioClip select obj as AudioClip).ToList();
        }

        private static List<AudioClip> GetListSfx()
        {
            if (!Directory.Exists(SoundDefine.PathSoundEffectSourceFolder))
            {
                return new List<AudioClip>();
            }

            var fileEntriesSoundEffect = Directory.GetFiles(SoundDefine.PathSoundEffectSourceFolder, "*", SearchOption.AllDirectories);
            return (from t in fileEntriesSoundEffect select ConvertSystemPathToUnityPath(t) into filePath select AssetDatabase.LoadAssetAtPath(filePath, typeof(object)) into obj where obj where obj is AudioClip select obj as AudioClip).ToList();
        }

#if SOUND_NAME_ENABLE
        [MenuItem("Tools/Sound/Generate")]
        public static void GenerateSoundName()
        {
            var typeBgm = TypeUtil.GetTypeByName("SoundBgmName");
            if (typeBgm != null)
            {
                GenerateBgm(typeBgm);
            }

            var typeSfx = TypeUtil.GetTypeByName("SoundSfxName");
            if (typeSfx != null)
            {
                GenerateSfx(typeSfx);
            }

            AssetDatabase.Refresh(ImportAssetOptions.ImportRecursive);
        }

        private static void GenerateBgm(Type typeBgm)
        {
            var bgmMaxNo = Enum.GetValues(typeBgm).Cast<int>().Max();
            var soundBgmFileName = Path.GetFileNameWithoutExtension(SoundDefine.PathSoundBgmName);
            var strBuilder = new StringBuilder();
            strBuilder.Append(DISABLE_CHECK_NAMESPACE).AppendLine();
            strBuilder.Append(DISABLE_CHECK_INCONSISTENTNAMING).AppendLine().AppendLine();
            var type = typeof(SoundGenerate);
            strBuilder.Append($"namespace {type.Namespace}").AppendLine();
            strBuilder.AppendLine("{");
            strBuilder.Append("\t").Append($"public enum {soundBgmFileName}").AppendLine();
            strBuilder.Append("\t").AppendLine("{");
            strBuilder.Append("\t\t").AppendFormat(@"None = 0,").AppendLine();
            var bgms = GetListBgm();
            var bgmNames = Enum.GetNames(typeBgm);
            foreach (var bgm in bgms)
            {
                var idx = (from t in bgmNames where t == bgm.name select (int) Enum.Parse(typeBgm, t)).FirstOrDefault();

                if (idx == 0)
                {
                    bgmMaxNo++;
                    idx = bgmMaxNo;
                }

                strBuilder.Append("\t\t").AppendFormat(@"{0},", bgm.name + " = " + idx).AppendLine();
            }

            strBuilder.Append("\t").AppendLine("}");
            strBuilder.AppendLine("}");
            strBuilder.AppendLine("\t");

            CreateDirectory(SoundDefine.PathSoundBgmName);
            File.WriteAllText(SoundDefine.PathSoundBgmName, strBuilder.ToString(), Encoding.UTF8);
        }

        private static void GenerateSfx(Type typeSfx)
        {
            var seMaxNo = Enum.GetValues(typeSfx).Cast<int>().Max();
            var soundSfxFileName = Path.GetFileNameWithoutExtension(SoundDefine.PathSoundSfxName);
            var strBuilder = new StringBuilder();
            strBuilder.Append(DISABLE_CHECK_NAMESPACE).AppendLine();
            strBuilder.Append(DISABLE_CHECK_INCONSISTENTNAMING).AppendLine().AppendLine();
            var type = typeof(SoundGenerate);
            strBuilder.Append($"namespace {type.Namespace}").AppendLine();
            strBuilder.AppendLine("{");
            strBuilder.Append("\t").Append($"public enum {soundSfxFileName}").AppendLine();
            strBuilder.Append("\t").AppendLine("{");
            strBuilder.Append("\t\t").AppendFormat(@"None = 0,").AppendLine();
            var soundEffects = GetListSfx();
            var sfxNames = Enum.GetNames(typeSfx);
            foreach (var sfx in soundEffects)
            {
                var idx = (from t in sfxNames where t == sfx.name select (int) Enum.Parse(typeSfx, t)).FirstOrDefault();

                if (idx == 0)
                {
                    seMaxNo++;
                    idx = seMaxNo;
                }

                strBuilder.Append("\t\t").AppendFormat(@"{0},", sfx.name + " = " + idx).AppendLine();
            }

            strBuilder.Append("\t").AppendLine("}");
            strBuilder.AppendLine("}");
            strBuilder.AppendLine("\t");

            CreateDirectory(SoundDefine.PathSoundSfxName);
            File.WriteAllText(SoundDefine.PathSoundSfxName, strBuilder.ToString(), Encoding.UTF8);
        }
#endif

#if !SOUND_NAME_ENABLE
        [MenuItem("Tools/Sound/CreateResources")]
        public static void CreateSoundName()
        {
            if (!Directory.Exists(SoundDefine.PathSoundBgmName))
            {
                CreateEmptySoundBgm();
            }

            if (!Directory.Exists(SoundDefine.PathSoundSfxName))
            {
                CreateEmptySoundSfx();
            }

            EditorUtil.AddDefineSymbols("SOUND_NAME_ENABLE");
        }

        private static void CreateEmptySoundBgm()
        {
            var soundBgmFileName = Path.GetFileNameWithoutExtension(SoundDefine.PathSoundBgmName);
            var strBuilder = new StringBuilder();
            strBuilder.Append(DISABLE_CHECK_NAMESPACE).AppendLine();
            strBuilder.Append(DISABLE_CHECK_INCONSISTENTNAMING).AppendLine().AppendLine();
            var type = typeof(SoundGenerate);
            strBuilder.Append($"namespace {type.Namespace}").AppendLine();
            strBuilder.AppendLine("{");
            strBuilder.Append("\t").Append($"public enum {soundBgmFileName}").AppendLine();
            strBuilder.Append("\t").AppendLine("{");
            strBuilder.Append("\t\t").AppendFormat(@"None = 0,").AppendLine();
            strBuilder.Append("\t").AppendLine("}");
            strBuilder.AppendLine("}");
            strBuilder.AppendLine("\t");
            CreateDirectory(SoundDefine.PathSoundBgmName);
            File.WriteAllText(SoundDefine.PathSoundBgmName, strBuilder.ToString(), Encoding.UTF8);
        }

        private static void CreateEmptySoundSfx()
        {
            var soundSfxFileName = Path.GetFileNameWithoutExtension(SoundDefine.PathSoundSfxName);
            var strBuilder = new StringBuilder();
            strBuilder.Append(DISABLE_CHECK_NAMESPACE).AppendLine();
            strBuilder.Append(DISABLE_CHECK_INCONSISTENTNAMING).AppendLine().AppendLine();
            var type = typeof(SoundGenerate);
            strBuilder.Append($"namespace {type.Namespace}").AppendLine();
            strBuilder.AppendLine("{");
            strBuilder.Append("\t").Append($"public enum {soundSfxFileName}").AppendLine();
            strBuilder.Append("\t").AppendLine("{");
            strBuilder.Append("\t\t").AppendFormat(@"None = 0,").AppendLine();
            strBuilder.Append("\t").AppendLine("}");
            strBuilder.AppendLine("}");
            strBuilder.AppendLine("\t");
            CreateDirectory(SoundDefine.PathSoundSfxName);
            File.WriteAllText(SoundDefine.PathSoundSfxName, strBuilder.ToString(), Encoding.UTF8);
        }
#endif

        private static void CreateDirectory(string path)
        {
            var directoryName = Path.GetDirectoryName(path);
            if (!Directory.Exists(directoryName) && !string.IsNullOrEmpty(directoryName))
            {
                Directory.CreateDirectory(directoryName);
            }
        }

        private static string ConvertSystemPathToUnityPath(string path)
        {
            var index = path.IndexOf("Assets", StringComparison.Ordinal);
            if (index > 0)
            {
                path = path.Remove(0, index);
            }

            var replace = path.Replace("\\", "/");
            Debug.Log("replace :" + replace);
            Debug.Log("path :" + path);
            return path;
        }
    }
}