# UniSound

## Installation

```bash
"com.yenmoc.unisound":"https://gitlab.com/yenmoc/unisound"
or
npm publish --registry=http://localhost:4873
```

## Install

Import package `SoundResource.unitypackage` form folder PackageResource located in the directory containing the module.
In `Root\SoundResources`
Create Folder `Bgm` to contain background music 
Create Folder `Sfx` to contain sound effect

## Usages

### BGM

#### Function
```csharp
public IAudioHandler PlayBgm(
            SoundBgmName soundName,
            float volume = DEFAULT_PARAM_VOLUME,
            float delay = DEFAULT_PARAM_DELAY,
            int loopCount = 1,
            float fadeInTime = 0f,
            float fadeOutTime = 0f,
            bool is3dSound = false,
            Action onStart = null,
            Action onPlay = null,
            Action onPause = null,
            Action onCompleted = null);


public IAudioHandler PlayBgm(
            SoundBgmName soundName,
            ISchedulerAudio scheduler,
            float volume = DEFAULT_PARAM_VOLUME,
            float delay = DEFAULT_PARAM_DELAY,
            int loopCount = 1,
            float fadeInTime = 0f,
            float fadeOutTime = 0f,
            bool is3dSound = false,
            Action onStart = null,
            Action onPlay = null,
            Action onPause = null,
            Action onCompleted = null);

public void ClearAllBgmCache(); //release all audio and remove them form cache (_cacheBgm)
public void ClearBgmCache(int id); // release audio with id and remove it form cache (_cacheBgm)
public void ClearBgmCache(SoundBgmName soundName);

public void StopAllBgm(); //stop all audio (with fadeout if have) and return them to pool
public void StopBgm(int id); //stop audio with id (with fadeout if have) and return it to pool
public void StopBgm(SoundBgmName soundName);

public void ForceStopAllBgm(); //stop all audio (without fadeout ) and return them to pool
public void ForceStopBgm(int id); //stop audio with id (without fadeout) and return it to pool
public void ForceStopBgm(SoundBgmName soundName);

public void ReleaseAllBgm(); //stop all audio (with fadeout if have) and return them to pool and release them form cache
public void ReleaseBgm(int id); //release audio with id (with fadeout if have) and remove it form cache (_cacheBgm) and release it form cache
public void ReleaseBgm(SoundBgmName soundName);

public void ForceReleaseAllBgm(); //force stop all audio (without fadeout) and return them to pool and release them form cache
public void ForceReleaseBgm(int id); //force stop audio with id (without fadeout) and return it to pool and release it form cache
public void ForceReleaseBgm(SoundBgmName soundName);

public void PauseAllBgm(); //pause all audio (with fadeout if have)
public void PauseBgm(int id); //pause audio with id (with fadeout if have)
public void PauseBgm(SoundBgmName soundName);

public void ForcePauseAllBgm(); //pause all audio (without fadeout)
public void ForcePauseBgm(int id); //pause audio with id (without fadeout)
public void ForcePauseBgm(SoundBgmName soundName);

public void ResumeAllBgm(); //resume all audio (with fadeout if have)
public void ResumeBgm(int id); //resume audio with id (with fadeout if have)
public void ResumeBgm(SoundBgmName soundName);


public IAudioHandler handler;
handler = soundManager.PlaySoundEffect(SoundSfxName.click, loopCount: 100);

handler.Pause();
handler.Resume();
handler.Stop(() => soundManager.SoundPool.Return((BaseAudio) handler));
```


```csharp
    public SoundManager soundManager;
    public IAudioHandler handler;

    // Start is called before the first frame update
    void Awake()
    {
        soundManager.Initialzied();
    }

    public void PlaySoundEffect()
    {
        handler = soundManager.PlaySoundEffect(SoundSfxName.bullet_impact_body_thump_02, fadeInTime: 0.1f, fadeOutTime: 0.1f);
    }

    public void PlaySoundEffect10()
    {
        handler = soundManager.PlaySoundEffect(SoundSfxName.bullet_impact_body_thump_02, fadeInTime: 0.1f, fadeOutTime: 0.1f, loopCount: 10);
    }

    public void PauseSoundEffect()
    {
        handler.Pause();
    }

    public void ResumeSoundEffect()
    {
        handler.Resume();
    }

    public void StopSoundEffect()
    {
        handler.Stop(() => soundManager.SoundPool.Return((BaseAudio) handler));
    }

    public void ReleaseAll()
    {
        soundManager.ReleaseAllSfx();
    }
```

