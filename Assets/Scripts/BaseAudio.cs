﻿using System;
using System.Collections;
using UniRx;
using UnityEngine;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CheckNamespace
namespace UnityModule.Sound
{
    public class BaseAudio : MonoBehaviour, IAudioHandler
    {
        #region properties

        public EAudioState PreviousState { get; set; }
        public EAudioState CurrentState { get; set; }
        public AudioSource Source { get; set; }
        public float Delay { get; set; }
        public int LoopCount { get; set; }
        public float FadeVolume { get; set; }
        public float FadeInTime { get; set; }
        public float FadeOutTime { get; set; }
        public IDisposable FadeDispose { get; set; }
        public IDisposable PlayDispose { get; set; }
        private float _waitTime;
        public ISchedulerAudio Scheduler { get; set; }
        public bool IsPlaying => CurrentState == EAudioState.Playing || CurrentState == EAudioState.AwaitPlaying;
        public bool IsPause => CurrentState == EAudioState.Pause || CurrentState == EAudioState.AwaitPause;
        private float _volume;

        public float Length
        {
            get
            {
                if (CurrentState == EAudioState.Stop || CurrentState == EAudioState.AwaitPlaying)
                {
                    return 0f;
                }

                return Mathf.Clamp01(Source.time / Source.clip.length);
            }
        }

        public float Volume
        {
            get => _volume;
            set => _volume = Mathf.Clamp01(value);
        }

        public Action onStart;
        public Action onPlay;
        public Action onPause;
        public Action onCompleted;

        #endregion

        #region function

        public void ChangeVolume()
        {
            Source.volume = Volume * FadeVolume;
        }

        // ReSharper disable once CognitiveComplexity
        private IEnumerator IePlay()
        {
            _waitTime = 0f;
            while (_waitTime < Delay)
            {
                _waitTime += Scheduler.DeltaTime;
                yield return null;
            }

            CurrentState = EAudioState.Playing;
            onPlay?.Invoke();
            Source.time = 0f;
            ChangeVolume();
            if (FadeInTime > 0f)
            {
                FadeIn(FadeInTime);
            }

            do
            {
                Source.Play();
                _waitTime = 0f;
                while (_waitTime < Source.clip.length / Source.pitch)
                {
                    if (CurrentState == EAudioState.Pause)
                    {
                        onPause?.Invoke();
                        while (CurrentState == EAudioState.Pause)
                        {
                            yield return null;
                        }
                    }

                    _waitTime += Scheduler.DeltaTime;
                    yield return null;
                }

                onCompleted?.Invoke();
                if (LoopCount > 0)
                {
                    LoopCount--;
                }
            } while (LoopCount == -1 || LoopCount > 0);

            //no dispose in here auto call dispose by subscribe to return to pool
            ResetPlayer();
        }

        public IObservable<Unit> Play()
        {
            gameObject.SetActive(true);
            CurrentState = EAudioState.AwaitPlaying;
            ChangeVolume();
            onStart?.Invoke();
            PlayDispose?.Dispose();
            return Observable.FromMicroCoroutine(IePlay).ForEachAsync(_ => PlayDispose?.Dispose());
        }

        public void Pause()
        {
            if (CurrentState != EAudioState.Playing && CurrentState != EAudioState.AwaitPlaying) return;
            PreviousState = CurrentState;
            CurrentState = EAudioState.AwaitPause;
            if (FadeOutTime > 0.0f)
            {
                FadeOut(FadeOutTime, OnPaused);
                return;
            }

            OnPaused();
        }

        public void Resume()
        {
            if (CurrentState != EAudioState.Pause && CurrentState != EAudioState.AwaitPause) return;
            CurrentState = PreviousState;
            Source.UnPause();
            FadeIn(FadeInTime);
        }

        public void Stop(Action action = null)
        {
            if (CurrentState == EAudioState.Stop || CurrentState == EAudioState.AwaitStop) return;
            CurrentState = EAudioState.AwaitStop;
            if (FadeOutTime > 0.0f)
            {
                FadeOut(FadeOutTime, () =>
                {
                    action?.Invoke();
                    OnStopped();
                });
                return;
            }

            action?.Invoke();
            OnStopped();
        }

        public void ForcePause()
        {
            if (CurrentState != EAudioState.Playing && CurrentState != EAudioState.AwaitPlaying) return;
            PreviousState = CurrentState;
            CurrentState = EAudioState.Pause;
            OnPaused();
        }

        public void ForceStop()
        {
            if (CurrentState == EAudioState.Stop) return;
            CurrentState = EAudioState.Stop;
            OnStopped();
        }

        private void OnPaused()
        {
            CurrentState = EAudioState.Pause;
            Source.Pause();
        }

        private void OnStopped()
        {
            CurrentState = EAudioState.Stop;
            Source.Stop();
            PlayDispose?.Dispose();
            FadeDispose?.Dispose();
            ResetPlayer();
        }

        protected void ResetPlayer()
        {
            gameObject.SetActive(false);
            CurrentState = EAudioState.Stop;
        }

        #region Fade

        protected void FadeIn(float fadeTime)
        {
            FadeDispose?.Dispose();
            FadeDispose = Observable.FromMicroCoroutine(() => IeFadeIn(fadeTime)).Subscribe((m) => FadeDispose?.Dispose()).AddTo(this);
        }

        protected void FadeOut(float fadeTime, Action onCompleted)
        {
            FadeDispose?.Dispose();
            FadeDispose = Observable.FromMicroCoroutine(() => IeFadeOut(fadeTime, onCompleted)).Subscribe((m) => FadeDispose?.Dispose()).AddTo(this);
        }

        private IEnumerator IeFadeIn(float fadeTime)
        {
            FadeVolume = 0;
            var time = 0f;
            while (time < fadeTime)
            {
                time += Scheduler.DeltaTime;
                FadeVolume = Mathf.Clamp01(time / fadeTime);
                ChangeVolume();
                yield return null;
            }

            FadeVolume = 1f;
            ChangeVolume();
        }

        private IEnumerator IeFadeOut(float fadeTime, Action onCompleted)
        {
            var time = 0f;
            while (time < fadeTime)
            {
                time += Scheduler.DeltaTime;
                FadeVolume = 1f - Mathf.Clamp01(time / fadeTime);
                ChangeVolume();
                yield return null;
            }

            FadeVolume = 0;
            ChangeVolume();
            onCompleted?.Invoke();
        }

        #endregion

        #endregion
    }
}