﻿// ReSharper disable CheckNamespace

namespace UnityModule.Sound
{
    public enum EAudioState
    {
        Stop = 0,
        Playing = 1,
        Pause = 2,
        AwaitPlaying = 3,
        AwaitPause = 4,
        AwaitStop = 5,
    }
}