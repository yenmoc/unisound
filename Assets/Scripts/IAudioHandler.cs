﻿using System;
using UniRx;

// ReSharper disable CheckNamespace
namespace UnityModule.Sound
{
    public interface IAudioHandler
    {
        EAudioState PreviousState { get; set; }
        EAudioState CurrentState { get; set; }
        UnityEngine.AudioSource Source { get; set; }
        float Delay { get; set; }
        int LoopCount { get; set; }
        float FadeVolume { get; set; }
        float FadeInTime { get; set; }
        float FadeOutTime { get; set; }
        IDisposable FadeDispose { get; set; }
        IDisposable PlayDispose { get; set; }
        ISchedulerAudio Scheduler { get; set; }
        void ChangeVolume();
        IObservable<Unit> Play();
        void Pause();
        void Resume();
        void Stop(Action action = null);
        void ForcePause();
        void ForceStop();
        float Volume { get; set; }
    }
}