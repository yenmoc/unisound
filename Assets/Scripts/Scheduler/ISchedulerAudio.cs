﻿// ReSharper disable CheckNamespace

namespace UnityModule.Sound
{
    public interface ISchedulerAudio
    {
        float DeltaTime { get; }
    }
}