﻿using UnityEngine;

// ReSharper disable CheckNamespace
namespace UnityModule.Sound
{
    public class TimeSchedulerAudioAudio : ISchedulerAudio
    {
        public float DeltaTime => Time.deltaTime;
    }
}