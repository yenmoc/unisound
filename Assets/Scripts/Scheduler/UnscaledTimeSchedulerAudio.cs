﻿using UnityEngine;

// ReSharper disable CheckNamespace
namespace UnityModule.Sound
{
    public class UnscaledTimeSchedulerAudio : ISchedulerAudio
    {
        public float DeltaTime => Time.unscaledDeltaTime;
    }
}