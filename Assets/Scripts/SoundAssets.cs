﻿using System;
using UnityEngine;

// ReSharper disable UnusedMember.Global
// ReSharper disable CheckNamespace
namespace UnityModule.Sound
{
    [CreateAssetMenu(fileName = "SoundAssets", menuName = "DataStorage/SoundAssets")]
    [Serializable]
    public class SoundAssets : ScriptableObject
    {
        [SerializeField] private AudioClip[] soundEffects;
        [SerializeField] private AudioClip[] backGroundMusics;

        public AudioClip[] BackGroundMusics
        {
            get => backGroundMusics;
            set => backGroundMusics = value;
        }

        public AudioClip[] SoundEffects
        {
            get => soundEffects;
            set => soundEffects = value;
        }
    }
}