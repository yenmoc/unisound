﻿// ReSharper disable CheckNamespace

namespace UnityModule.Sound
{
    public static class SoundDefine
    {
        public static string PathBgmSourceFolder => "Assets/Root/SoundResources/Bgm";
        public static string PathSoundEffectSourceFolder => "Assets/Root/SoundResources/Sfx";
        public static string PathSoundBgmName => "Assets/Root/SoundResources/SoundBgmName.cs";
        public static string PathSoundSfxName => "Assets/Root/SoundResources/SoundSfxName.cs";
    }
}