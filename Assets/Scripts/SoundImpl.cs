﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Async;
using UnityEngine;

// ReSharper disable ForCanBeConvertedToForeach
namespace UnityModule.Sound
{
    public class SoundImpl
    {
        #region properties

        private readonly BaseAudio _soundPrefab;
        private readonly Transform _rootParent;
        public float VolumeBackground { get; set; }
        public float VolumeSoundEffect { get; set; }
        public bool IsBackgroundMusic { get; set; }
        public bool IsSoundEffect { get; set; }

        private readonly Dictionary<int, AudioClip> _cacheSfx = new Dictionary<int, AudioClip>();
        private readonly Dictionary<int, AudioClip> _cacheBgm = new Dictionary<int, AudioClip>();
        private readonly List<BaseAudio> _cacheSfxObject = new List<BaseAudio>();
        private readonly Dictionary<int, BaseAudio> _cacheBgmObject = new Dictionary<int, BaseAudio>();
        public SoundPool SoundPool { get; private set; }
        private readonly ISchedulerAudio _defaultScheduler = new TimeSchedulerAudioAudio();

        #endregion

        #region function

        public SoundImpl(BaseAudio soundPrefab, Transform rootParent = null)
        {
            _soundPrefab = soundPrefab;
            _rootParent = rootParent;
        }

        public SoundImpl()
        {
        }

        public void Initialized()
        {
            SoundPool = new SoundPool(_soundPrefab, _rootParent);
        }

        #endregion

        #region sfx

        public IAudioHandler PlaySoundEffect(
            string soundName,
            int soundId,
            float volume = 1f,
            float delay = 0f,
            int loopCount = 1,
            float fadeInTime = 0f,
            float fadeOutTime = 0f,
            bool is3dSound = false,
            Vector3 position = default,
            Action onStart = null,
            Action onPlay = null,
            Action onPause = null,
            Action onCompleted = null)
        {
            return soundId == 0 ? null : PlaySoundEffectImpl(soundName, soundId, _defaultScheduler, volume, delay, loopCount, fadeInTime, fadeOutTime, is3dSound, position, onStart, onPlay, onPause, onCompleted);
        }

        public IAudioHandler PlaySoundEffect(
            string soundName,
            int soundId,
            ISchedulerAudio scheduler,
            float volume = 1f,
            float delay = 0f,
            int loopCount = 1,
            float fadeInTime = 0f,
            float fadeOutTime = 0f,
            bool is3dSound = false,
            Vector3 position = default,
            Action onStart = null,
            Action onPlay = null,
            Action onPause = null,
            Action onCompleted = null)
        {
            return soundId == 0 ? null : PlaySoundEffectImpl(soundName, soundId, scheduler, volume, delay, loopCount, fadeInTime, fadeOutTime, is3dSound, position, onStart, onPlay, onPause, onCompleted);
        }

        private IAudioHandler PlaySoundEffectImpl(
            string soundName,
            int soundId,
            ISchedulerAudio scheduler,
            float volume,
            float delay,
            int loopCount,
            float fadeInTime,
            float fadeOutTime,
            bool is3dSound,
            Vector3 soundPosition,
            Action onStart,
            Action onPlay,
            Action onPause,
            Action onCompleted)
        {
            if (!IsSoundEffect)
            {
                return null;
            }

            IAudioHandler handler = null;
            SoundPool.RentAsync().Subscribe(async sound =>
            {
                handler = sound;
                if (sound.Source == null)
                {
                    sound.Source = sound.GetComponent<AudioSource>();
                }

                sound.Source.playOnAwake = false;
                sound.Source.volume = 0;
                sound.CurrentState = EAudioState.Stop;
                sound.FadeVolume = 1;
                sound.Scheduler = scheduler;
                sound.transform.position = soundPosition;
                sound.Source.spatialBlend = is3dSound ? 1f : 0f;
                sound.Source.rolloffMode = is3dSound ? AudioRolloffMode.Linear : AudioRolloffMode.Logarithmic;
                sound.LoopCount = loopCount;
                sound.Volume = volume * Mathf.Clamp01(VolumeSoundEffect);
                sound.Delay = delay;
                sound.onStart = onStart;
                sound.onPlay = onPlay;
                sound.onPause = onPause;
                sound.onCompleted = onCompleted;

                fadeInTime = Mathf.Clamp(fadeInTime, 0f, float.MaxValue);
                fadeOutTime = Mathf.Clamp(fadeOutTime, 0f, float.MaxValue);

                if (!_cacheSfx.ContainsKey(soundId))
                {
                    var task = LazyAddressable.LoadAddressable<AudioClip>(soundName);
                    _cacheSfx.Add(soundId, await task);
                }

                sound.Source.clip = _cacheSfx[soundId];
                _cacheSfxObject.Add(sound);
                sound.PlayDispose = sound.Play().Subscribe(_ =>
                {
                    _cacheSfxObject.Remove(sound);
                    SoundPool.Return(sound);
                }).AddTo(sound);
            });
            return handler;
        }

        /// <summary>
        /// release all cache sfx and clear dictionary _cacheSfx
        /// </summary>
        public void ClearAllSfxCache()
        {
            var keys = _cacheSfx.Keys.ToArray();
            for (int i = 0; i < keys.Length; i++)
            {
                UnityEngine.AddressableAssets.Addressables.Release(_cacheSfx[keys[i]]);
            }

            _cacheSfx.Clear();
        }

        /// <summary>
        /// release one element cache sfx and remove it form dictionary _cacheSfx
        /// </summary>
        /// <param name="id">id sound need release and remove</param>
        public void ClearSfxCache(int id)
        {
            if (!_cacheSfx.ContainsKey(id)) return;
            UnityEngine.AddressableAssets.Addressables.Release(_cacheSfx[id]);
            _cacheSfx.Remove(id);
        }

        /// <summary>
        /// stop, return to pool and release all cache sfx and clear dictionary _cacheSfx
        /// </summary>
        public void ReleaseAllSfx()
        {
            for (int i = 0; i < _cacheSfxObject.Count; i++)
            {
                var sfx = _cacheSfxObject[i];
                sfx.Stop(() => SoundPool.Return(sfx));
            }

            _cacheSfxObject.Clear();
            ClearAllSfxCache();
        }

        #endregion

        #region bgm

        public IAudioHandler PlayBgm(
            string soundName,
            int soundId,
            float volume = 1f,
            float delay = 0f,
            int loopCount = 1,
            float fadeInTime = 0f,
            float fadeOutTime = 0f,
            bool is3dSound = false,
            Vector3 positon = default,
            Action onStart = null,
            Action onPlay = null,
            Action onPause = null,
            Action onCompleted = null)
        {
            return soundId == 0 ? null : PlayBgmImpl(soundName, soundId, _defaultScheduler, volume, delay, loopCount, fadeInTime, fadeOutTime, is3dSound, positon, onStart, onPlay, onPause, onCompleted);
        }

        public IAudioHandler PlayBgm(
            string soundName,
            int soundId,
            ISchedulerAudio scheduler,
            float volume = 1f,
            float delay = 0f,
            int loopCount = 1,
            float fadeInTime = 0f,
            float fadeOutTime = 0f,
            bool is3dSound = false,
            Vector3 position = default,
            Action onStart = null,
            Action onPlay = null,
            Action onPause = null,
            Action onCompleted = null)
        {
            return soundId == 0 ? null : PlayBgmImpl(soundName, soundId, scheduler, volume, delay, loopCount, fadeInTime, fadeOutTime, is3dSound, position, onStart, onPlay, onPause, onCompleted);
        }

        private IAudioHandler PlayBgmImpl(
            string soundName,
            int soundId,
            ISchedulerAudio scheduler,
            float volume,
            float delay,
            int loopCount,
            float fadeInTime,
            float fadeOutTime,
            bool is3dSound,
            Vector3 soundPosition,
            Action onStart,
            Action onPlay,
            Action onPause,
            Action onCompleted)
        {
            if (!IsBackgroundMusic)
            {
                return null;
            }

            IAudioHandler handler = null;
            SoundPool.RentAsync().Subscribe(async sound =>
            {
                handler = sound;
                if (sound.Source == null)
                {
                    sound.Source = sound.GetComponent<AudioSource>();
                }

                sound.Source.playOnAwake = false;
                sound.Source.volume = 0;
                sound.CurrentState = EAudioState.Stop;
                sound.FadeVolume = 1;
                sound.Scheduler = scheduler;
                sound.transform.position = soundPosition;
                sound.Source.spatialBlend = is3dSound ? 1f : 0f;
                sound.Source.rolloffMode = is3dSound ? AudioRolloffMode.Linear : AudioRolloffMode.Logarithmic;
                sound.LoopCount = loopCount;
                sound.Volume = volume * Mathf.Clamp01(VolumeBackground);
                sound.Delay = delay;
                sound.onStart = onStart;
                sound.onPlay = onPlay;
                sound.onPause = onPause;
                sound.onCompleted = onCompleted;

                sound.FadeInTime = Mathf.Clamp(fadeInTime, 0f, float.MaxValue);
                sound.FadeOutTime = Mathf.Clamp(fadeOutTime, 0f, float.MaxValue);

                if (!_cacheBgm.ContainsKey(soundId))
                {
                    var task = LazyAddressable.LoadAddressable<AudioClip>(soundName);
                    _cacheBgm.Add(soundId, await task);
                }

                sound.Source.clip = _cacheBgm[soundId];
                if (!_cacheBgmObject.ContainsKey(soundId))
                {
                    _cacheBgmObject.Add(soundId, sound);
                }
                else
                {
                    if (_cacheBgmObject[soundId] != null && _cacheBgmObject[soundId].CurrentState != EAudioState.Stop)
                    {
                        //stop previous same track
                        _cacheBgmObject[soundId].ForceStop();
                        SoundPool.Return(_cacheBgmObject[soundId]);
                    }

                    _cacheBgmObject[soundId] = sound;
                }

                sound.PlayDispose = sound.Play().Subscribe(_ => SoundPool.Return(sound)).AddTo(sound);
            });
            return handler;
        }

        /// <summary>
        /// release all cache bgm and clear dictionary _cacheBgm
        /// </summary>
        public void ClearAllBgmCache()
        {
            var keys = _cacheBgm.Keys.ToArray();
            for (int i = 0; i < keys.Length; i++)
            {
                UnityEngine.AddressableAssets.Addressables.Release(_cacheBgm[keys[i]]);
            }

            _cacheBgm.Clear();
        }

        /// <summary>
        /// release one element cache bgm and remove it form dictionary _cacheBgm
        /// </summary>
        /// <param name="id">id sound need release and remove</param>
        public void ClearBgmCache(int id)
        {
            if (!_cacheBgm.ContainsKey(id)) return;
            UnityEngine.AddressableAssets.Addressables.Release(_cacheBgm[id]);
            _cacheBgm.Remove(id);
        }


        /// <summary>
        /// stop all background music
        /// </summary>
        public void StopAllBgm()
        {
            var keys = _cacheBgmObject.Keys.ToArray();
            for (int i = 0; i < keys.Length; i++)
            {
                var id = keys[i];
                _cacheBgmObject[id].Stop(() => SoundPool.Return(_cacheBgmObject[id]));
            }
        }

        /// <summary>
        /// stop one background music
        /// </summary>
        /// <param name="id">id sound need stop</param>
        public void StopBgm(int id)
        {
            if (!_cacheBgmObject.ContainsKey(id)) return;
            _cacheBgmObject[id].Stop(() => SoundPool.Return(_cacheBgmObject[id]));
        }


        /// <summary>
        /// force stop all background music
        /// </summary>
        public void ForceStopAllBgm()
        {
            var keys = _cacheBgmObject.Keys.ToArray();
            for (int i = 0; i < keys.Length; i++)
            {
                var id = keys[i];
                if (_cacheBgmObject[id].CurrentState == EAudioState.Stop) continue;
                _cacheBgmObject[id].ForceStop();
                SoundPool.Return(_cacheBgmObject[id]);
            }
        }

        /// <summary>
        /// force stop one background music
        /// </summary>
        /// <param name="id">id sound need stop</param>
        public void ForceStopBgm(int id)
        {
            if (!_cacheBgmObject.ContainsKey(id) || _cacheBgmObject[id].CurrentState == EAudioState.Stop) return;
            _cacheBgmObject[id].ForceStop();
            SoundPool.Return(_cacheBgmObject[id]);
        }


        /// <summary>
        /// stop all background music and release them when stopped
        /// </summary>
        public void ReleaseAllBgm()
        {
            var keys = _cacheBgmObject.Keys.ToArray();
            for (int i = 0; i < keys.Length; i++)
            {
                var id = keys[i];
                _cacheBgmObject[id].Stop(() =>
                {
                    SoundPool.Return(_cacheBgmObject[id]);
                    ClearBgmCache(id);
                });
            }
        }

        /// <summary>
        /// stop and release one background music
        /// </summary>
        /// <param name="id">id sound need stop</param>
        public void ReleaseBgm(int id)
        {
            if (!_cacheBgmObject.ContainsKey(id)) return;
            _cacheBgmObject[id].Stop(() =>
            {
                SoundPool.Return(_cacheBgmObject[id]);
                ClearBgmCache(id);
            });
        }


        /// <summary>
        /// force stop all background music and release them when stopped
        /// </summary>
        public void ForceReleaseAllBgm()
        {
            var keys = _cacheBgmObject.Keys.ToArray();
            for (int i = 0; i < keys.Length; i++)
            {
                var id = keys[i];
                if (_cacheBgmObject[id].CurrentState == EAudioState.Stop) continue;
                _cacheBgmObject[id].ForceStop();
                SoundPool.Return(_cacheBgmObject[id]);
                ClearBgmCache(id);
            }
        }

        /// <summary>
        /// force stop and release one background music
        /// </summary>
        /// <param name="id">id sound need stop</param>
        public void ForceReleaseBgm(int id)
        {
            if (!_cacheBgmObject.ContainsKey(id) || _cacheBgmObject[id].CurrentState == EAudioState.Stop) return;
            _cacheBgmObject[id].ForceStop();
            SoundPool.Return(_cacheBgmObject[id]);
            ClearBgmCache(id);
        }


        /// <summary>
        /// pause all background music
        /// </summary>
        public void PauseAllBgm()
        {
            var keys = _cacheBgmObject.Keys.ToArray();
            for (int i = 0; i < keys.Length; i++)
            {
                _cacheBgmObject[keys[i]].Pause();
            }
        }

        /// <summary>
        /// pause background music with id
        /// </summary>
        /// <param name="id"></param>
        public void PauseBgm(int id)
        {
            if (!_cacheBgmObject.ContainsKey(id)) return;
            _cacheBgmObject[id].Pause();
        }


        /// <summary>
        /// force pause all background music
        /// </summary>
        public void ForcePauseAllBgm()
        {
            var keys = _cacheBgmObject.Keys.ToArray();
            for (int i = 0; i < keys.Length; i++)
            {
                _cacheBgmObject[keys[i]].ForcePause();
            }
        }

        /// <summary>
        /// force pause background music with id
        /// </summary>
        /// <param name="id"></param>
        public void ForcePauseBgm(int id)
        {
            if (!_cacheBgmObject.ContainsKey(id)) return;
            _cacheBgmObject[id].ForcePause();
        }


        /// <summary>
        /// resume all background music
        /// </summary>
        public void ResumeAllBgm()
        {
            var keys = _cacheBgmObject.Keys.ToArray();
            for (int i = 0; i < keys.Length; i++)
            {
                _cacheBgmObject[keys[i]].Resume();
            }
        }

        /// <summary>
        /// resume background music with id
        /// </summary>
        /// <param name="id"></param>
        public void ResumeBgm(int id)
        {
            if (!_cacheBgmObject.ContainsKey(id)) return;
            _cacheBgmObject[id].Resume();
        }

        #endregion
    }
}