﻿using System;
using UnityEngine;

namespace UnityModule.Sound
{
    public partial class SoundManager : MonoBehaviour
    {
        private SoundImpl _soundImpl;
        [SerializeField] private BaseAudio soundPrefab = null;
        [Range(0f, 1f), SerializeField] private float volumeBackground = 1f;
        [Range(0f, 1f), SerializeField] private float volumeSoundEffect = 1f;
        [SerializeField] private bool isBackgroundMusic = true;
        [SerializeField] private bool isSoundEffect = true;

        public float VolumeBackground
        {
            get => volumeBackground;
            set
            {
                volumeBackground = value;
                if (_soundImpl != null)
                {
                    _soundImpl.VolumeBackground = value;
                }
            }
        }

        public float VolumeSoundEffect
        {
            get => volumeSoundEffect;
            set
            {
                volumeSoundEffect = value;
                if (_soundImpl != null)
                {
                    _soundImpl.VolumeSoundEffect = value;
                }
            }
        }

        public bool IsBacgroundMusic
        {
            get => isBackgroundMusic;
            set
            {
                isBackgroundMusic = value;
                if (_soundImpl != null)
                {
                    _soundImpl.IsBackgroundMusic = value;
                }
            }
        }

        public bool IsSoundEffect
        {
            get => isSoundEffect;
            set
            {
                isSoundEffect = value;
                if (_soundImpl != null)
                {
                    _soundImpl.IsSoundEffect = value;
                }
            }
        }

        public SoundPool SoundPool => _soundImpl.SoundPool;

        public void Initialzied(bool isBackgroundMusic = true, bool isSoundEffect = true, float volumeBackground = 1f, float volumeSoundEffect = 1f)
        {
            _soundImpl = new SoundImpl(soundPrefab, transform);
            _soundImpl.Initialized();
            VolumeBackground = volumeBackground;
            VolumeSoundEffect = volumeSoundEffect;
            IsBacgroundMusic = isBackgroundMusic;
            IsSoundEffect = isSoundEffect;
        }

        public void PauseAll()
        {
            AudioListener.pause = true;
        }

        public void ResumeAll()
        {
            AudioListener.pause = false;
        }

        public IAudioHandler PlaySoundEffect(
            string soundName,
            int soundId,
            float volume = 1f,
            float delay = 0f,
            int loopCount = 1,
            float fadeInTime = 0f,
            float fadeOutTime = 0f,
            bool is3dSound = false,
            Vector3 position = default,
            Action onStart = null,
            Action onPlay = null,
            Action onPause = null,
            Action onCompleted = null)
        {
            return _soundImpl.PlaySoundEffect(soundName, soundId, volume, delay, loopCount, fadeInTime, fadeOutTime, is3dSound, position, onStart, onPlay, onPause, onCompleted);
        }

        public IAudioHandler PlaySoundEffect(
            string soundName,
            int soundId,
            ISchedulerAudio scheduler,
            float volume = 1f,
            float delay = 0f,
            int loopCount = 1,
            float fadeInTime = 0f,
            float fadeOutTime = 0f,
            bool is3dSound = false,
            Vector3 position = default,
            Action onStart = null,
            Action onPlay = null,
            Action onPause = null,
            Action onCompleted = null)
        {
            return _soundImpl.PlaySoundEffect(soundName, soundId, scheduler, volume, delay, loopCount, fadeInTime, fadeOutTime, is3dSound, position, onStart, onPlay, onPause, onCompleted);
        }

        /// <summary>
        /// release all cache sfx and clear dictionary _cacheSfx
        /// </summary>
        public void ClearAllSfxCache()
        {
            _soundImpl.ClearAllSfxCache();
        }

        /// <summary>
        /// release one element cache sfx and remove it form dictionary _cacheSfx
        /// </summary>
        /// <param name="id">id sound need release and remove</param>
        public void ClearSfxCache(int id)
        {
            _soundImpl.ClearSfxCache(id);
        }

        /// <summary>
        /// stop, return to pool and release all cache sfx and clear dictionary _cacheSfx
        /// </summary>
        public void ReleaseAllSfx()
        {
            _soundImpl.ReleaseAllSfx();
        }

        public IAudioHandler PlayBgm(
            string soundName,
            int soundId,
            float volume = 1f,
            float delay = 0f,
            int loopCount = 1,
            float fadeInTime = 0f,
            float fadeOutTime = 0f,
            bool is3dSound = false,
            Vector3 position = default,
            Action onStart = null,
            Action onPlay = null,
            Action onPause = null,
            Action onCompleted = null)
        {
            return _soundImpl.PlayBgm(soundName, soundId, volume, delay, loopCount, fadeInTime, fadeOutTime, is3dSound, position, onStart, onPlay, onPause, onCompleted);
        }

        public IAudioHandler PlayBgm(
            string soundName,
            int soundId,
            ISchedulerAudio scheduler,
            float volume = 1f,
            float delay = 0f,
            int loopCount = 1,
            float fadeInTime = 0f,
            float fadeOutTime = 0f,
            bool is3dSound = false,
            Vector3 position = default,
            Action onStart = null,
            Action onPlay = null,
            Action onPause = null,
            Action onCompleted = null)
        {
            return _soundImpl.PlayBgm(soundName, soundId, scheduler, volume, delay, loopCount, fadeInTime, fadeOutTime, is3dSound, position, onStart, onPlay, onPause, onCompleted);
        }

        /// <summary>
        /// release all cache bgm and clear dictionary _cacheBgm
        /// </summary>
        public void ClearAllBgmCache()
        {
            _soundImpl.ClearAllBgmCache();
        }

        /// <summary>
        /// release one element cache bgm and remove it form dictionary _cacheBgm
        /// </summary>
        /// <param name="id">id sound need release and remove</param>
        public void ClearBgmCache(int id)
        {
            _soundImpl.ClearBgmCache(id);
        }

        /// <summary>
        /// stop all background music
        /// </summary>
        public void StopAllBgm()
        {
            _soundImpl.StopAllBgm();
        }

        /// <summary>
        /// stop one background music
        /// </summary>
        /// <param name="id">id sound need stop</param>
        public void StopBgm(int id)
        {
            _soundImpl.StopBgm(id);
        }

        /// <summary>
        /// force stop all background music
        /// </summary>
        public void ForceStopAllBgm()
        {
            _soundImpl.ForceStopAllBgm();
        }

        /// <summary>
        /// force stop one background music
        /// </summary>
        /// <param name="id">id sound need stop</param>
        public void ForceStopBgm(int id)
        {
            _soundImpl.ForceStopBgm(id);
        }

        /// <summary>
        /// stop all background music and release them when stopped
        /// </summary>
        public void ReleaseAllBgm()
        {
            _soundImpl.ReleaseAllBgm();
        }

        /// <summary>
        /// stop and release one background music
        /// </summary>
        /// <param name="id">id sound need stop</param>
        public void ReleaseBgm(int id)
        {
            _soundImpl.ReleaseBgm(id);
        }

        /// <summary>
        /// force stop all background music and release them when stopped
        /// </summary>
        public void ForceReleaseAllBgm()
        {
            _soundImpl.ForceReleaseAllBgm();
        }

        /// <summary>
        /// force stop and release one background music
        /// </summary>
        /// <param name="id">id sound need stop</param>
        public void ForceReleaseBgm(int id)
        {
            _soundImpl.ForceReleaseBgm(id);
        }

        /// <summary>
        /// pause all background music
        /// </summary>
        public void PauseAllBgm()
        {
            _soundImpl.PauseAllBgm();
        }

        /// <summary>
        /// pause background music with id
        /// </summary>
        /// <param name="id"></param>
        public void PauseBgm(int id)
        {
            _soundImpl.PauseBgm(id);
        }

        /// <summary>
        /// force pause all background music
        /// </summary>
        public void ForcePauseAllBgm()
        {
            _soundImpl.ForcePauseAllBgm();
        }

        /// <summary>
        /// force pause background music with id
        /// </summary>
        /// <param name="id"></param>
        public void ForcePauseBgm(int id)
        {
            _soundImpl.ForcePauseBgm(id);
        }

        /// <summary>
        /// resume all background music
        /// </summary>
        public void ResumeAllBgm()
        {
            _soundImpl.ResumeAllBgm();
        }

        /// <summary>
        /// resume background music with id
        /// </summary>
        /// <param name="id"></param>
        public void ResumeBgm(int id)
        {
            _soundImpl.ResumeBgm(id);
        }
    }
}