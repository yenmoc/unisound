﻿using System;
using UniRx;
using UniRx.Toolkit;
using UnityEngine;

// ReSharper disable CheckNamespace
namespace UnityModule.Sound
{
    public class SoundPool : AsyncObjectPool<BaseAudio>
    {
        private readonly BaseAudio _player;
        private readonly Transform _parent;

        public SoundPool(BaseAudio player, Transform parent)
        {
            _player = player;
            _parent = parent;
        }

        protected override IObservable<BaseAudio> CreateInstanceAsync()
        {
            var sound = UnityEngine.Object.Instantiate(_player, _parent, false);
            return Observable.Return(sound);
        }
        
        protected override void OnBeforeReturn(BaseAudio instance)
        {
            base.OnBeforeReturn(instance);
            instance.Source.clip = null;
        }
    }
}